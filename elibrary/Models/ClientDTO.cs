﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace elibrary.Models
{
    public class ClientDTO
    {
        public int Id { get; set; }
        public string Number { get; set; }
        public DateTime Date { get; set; }
        public int Sum { get; set; }
        public int Profit { get; set; }
        public bool IsDelivered { get; set; } = false;
        public bool IsChecked { get; set; } = false;

        public string ShopName { get; set; }
        public string SellerName { get; set; }
        public string MyNumber { get; set; }
    }
}
