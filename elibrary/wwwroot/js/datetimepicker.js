﻿
$(function () {
    $('.flt-datetimepicker').datetimepicker({
        format: 'd.m.Y H:i',
        defaultDate: new Date(),
        defaultTime: '09:00',
        step: 20,
        minTime: '08:00',
        maxTime: '21:00',

        onSelectDate: function (ct, $i) {
            if (typeof reloadData === 'function') {
                reloadData();
            } else {
                //alert(['Данный файл используется для привязки контрола даты.',
                //    'После выбора даты, подразумевается вызов функции reloadData,',
                //    'которая инициирует перезагрузку данных.',
                //    'Судя по всему, у вас нет такой функции, или вы ее забыли добавить.'].join(' '));
            }
        }
    });
    //$('.datetimepicker_end').datetimepicker({
    //    format: 'd.m.Y H:i',
    //    defaultDate: new Date(),
    //    defaultTime: '18:00',
    //    step: 20,
    //    minTime: '08:00',
    //    maxTime: '21:00'
    //});
});