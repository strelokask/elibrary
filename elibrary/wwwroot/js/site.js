﻿// Please see documentation at https://docs.microsoft.com/aspnet/core/client-side/bundling-and-minification
// for details on configuring this project to bundle and minify static web assets.

// Write your JavaScript code.

//init mvc grid
(function () {

    [].forEach.call(document.getElementsByClassName('mvc-grid'), function (element) {
        new MvcGrid(element);
    });

})()


$(function () {

    $('.btn-report-download').on('click', function (e) {
        let format = $(e.currentTarget).data('format');
        $('#format').val(format);
        $('#format').closest('form').submit();
    })

});