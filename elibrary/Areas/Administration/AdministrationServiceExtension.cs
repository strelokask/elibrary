﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace elibrary.Areas.Administration
{
    public static class AdministrationServiceExtension
    {
        public static void RegisterBackendServices(this IServiceCollection services)
        {

            services.AddTransient<Services.ShopService>();
            services.AddTransient<Services.SellerService>();
            services.AddTransient<Services.NumberService>();
        }
    }
}
