﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace elibrary.Areas.Administration.Models
{
    public class NumberDTO
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Заполните номер телефона")]
        [RegularExpression(@"^[+]\d{11}$", ErrorMessage = "Номер телефон не действителен. Пример: +77051234567")]
        [Display(Name ="Номер телефона")]
        public string PhoneNumber { get; set; }

        [Display(Name = "Дата последнего заполнения")]
        [DataType(DataType.DateTime)]
        public DateTime Date { get; set; }

        [Display(Name = "Сумма последнего заполнения")]
        public int Sum { get; set; }

        [Display(Name = "Остаток")]
        public int Balance { get; set; }
    }
}
