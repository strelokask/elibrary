﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace elibrary.Areas.Administration.Models
{
    public class ShopDTO
    {
        public int Id { get; set; }

        [Display(Name = "Название магазина")]
        public string Name { get; set; }

    }
}
