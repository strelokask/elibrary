﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace elibrary.Areas.Administration.Models
{
    public class SellerDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
