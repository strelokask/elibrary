﻿using System.Threading.Tasks;
using elibrary.Areas.Administration.Services;
using Microsoft.AspNetCore.Mvc;
using elibrary.Areas.Administration.Models;

namespace elibrary.Areas.Administration.Controllers
{
    public class ShopController : AdministrationDefaultController
    {
        private ShopService _service;

        public ShopController(ShopService service)
        {
            _service = service;
        }
        public async Task<IActionResult> Index()
        {
            var model = await _service.GetCollectionDTOAsync();

            return View(model);
        }

        public IActionResult Create() => View(new ShopDTO());

        public async Task<IActionResult> Edit(int id)
        {
            ViewData["Title"] = "Изменить";
            var model = await _service.GetShopDTOAsync(id);

            return View(nameof(Create), model);
        }

        public async Task<IActionResult> Delete(int id)
        {
            await _service.DeleteAsync(id);
            return RedirectToAction(nameof(Index));
        }

        [HttpPost]
        [ActionName(nameof(Edit))]
        public async Task<IActionResult> EditPost(ShopDTO dto)
        {
            if (ModelState.IsValid)
            {
                await _service.Save(dto);
                //_flasher.Flash(FlashTypes.Success, "Данные сохранены");

                return RedirectToAction(nameof(Index));
            }

            ViewData["Title"] = "Изменить";
            return View(nameof(Create), dto);
        }
    }
}