﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Core.Flash;
using elibrary.Areas.Administration.Models;
using elibrary.Areas.Administration.Services;
using elibrary.Classes;
using Microsoft.AspNetCore.Mvc;

namespace elibrary.Areas.Administration.Controllers
{
    public class SellerController : AdministrationDefaultController
    {
        private SellerService _service;
        private IFlasher _flasher;

        public SellerController(SellerService service, IFlasher flasher)
        {
            _service = service;
            _flasher = flasher;
        }
        public async Task<IActionResult> Index()
        {
            var model = await _service.GetCollectionDTOAsync();

            return View(model);
        }

        public IActionResult Create() => View(new SellerDTO());

        public async Task<IActionResult> Edit(int id)
        {
            ViewData["Title"] = "Изменить";
            var model = await _service.GetDTOAsync(id);

            return View(nameof(Create), model);
        }

        public async Task<IActionResult> Delete(int id)
        {
            await _service.DeleteAsync(id);
            _flasher.Flash(FlashTypes.Success, "Данные удалены");
            return RedirectToAction(nameof(Index));
        }

        [HttpPost]
        [ActionName(nameof(Edit))]
        public async Task<IActionResult> EditPost(SellerDTO dto)
        {
            if (ModelState.IsValid)
            {
                await _service.Save(dto);
                _flasher.Flash(FlashTypes.Success, "Данные сохранены");
                return RedirectToAction(nameof(Index));
            }

            ViewData["Title"] = "Изменить";
            return View(nameof(Create), dto);
        }
    }
}