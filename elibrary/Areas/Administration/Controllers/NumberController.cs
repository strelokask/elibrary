﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using elibrary.Areas.Administration.Models;
using elibrary.Areas.Administration.Services;
using Microsoft.AspNetCore.Mvc;

namespace elibrary.Areas.Administration.Controllers
{
    public class NumberController : AdministrationDefaultController
    {
        private NumberService _service;

        public NumberController(NumberService service)
        {
            _service = service;
        }
        public async Task<IActionResult> Index()
        {
            var model = await _service.GetCollectionDTOAsync();

            return View(model);
        }

        public IActionResult Create() => View(new NumberDTO());

        public async Task<IActionResult> Edit(int id)
        {
            ViewData["Title"] = "Изменить";
            var model = await _service.GetDTOAsync(id);

            return View(nameof(Create), model);
        }

        public async Task<IActionResult> Delete(int id)
        {
            await _service.DeleteAsync(id);
            return RedirectToAction(nameof(Index));
        }

        [HttpPost]
        [ActionName(nameof(Edit))]
        public async Task<IActionResult> EditPost(NumberDTO dto)
        {
            if (ModelState.IsValid)
            {
                await _service.Save(dto);
                //_flasher.Flash(FlashTypes.Success, "Данные сохранены");

                return RedirectToAction(nameof(Index));
            }

            ViewData["Title"] = "Изменить";
            return View(nameof(Create), dto);
        }
    }
}