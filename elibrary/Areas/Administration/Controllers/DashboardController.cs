﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace elibrary.Areas.Administration.Controllers
{
    public class DashboardController : AdministrationDefaultController
    {
        public IActionResult Index()
        {
            return View();
        }
    }
}