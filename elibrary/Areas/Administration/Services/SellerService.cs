﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using elibrary.Areas.Administration.Models;
using elibrary.Data;
using Microsoft.EntityFrameworkCore;

namespace elibrary.Areas.Administration.Services
{
    public class SellerService
    {
        private readonly ApplicationDbContext _context;
        public SellerService(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<SellerDTO>> GetCollectionDTOAsync()
        {
            return await _context.Sellers.Select(x => new SellerDTO { Id = x.Id, Name = x.Name }).ToListAsync();
        }

        public async Task<SellerDTO> GetDTOAsync(int id)
        {
            return await _context.Sellers.Select(x => new SellerDTO { Id = x.Id, Name = x.Name }).FirstAsync();
        }

        public async Task DeleteAsync(int id)
        {
            var a = await _context.Sellers.Where(x => x.Id == id).FirstAsync();

            _context.Sellers.Remove(a);

            _context.SaveChanges();
        }

        public async Task Save(SellerDTO dto)
        {
            if (dto.Id > 0)
            {
                var shop = await _context.Sellers
                .Where(x => x.Id == dto.Id)
                .FirstAsync();

                shop.Name = dto.Name;
                _context.Sellers.Update(shop);
            }
            else
            {
                var shop = new elibrary.Data.Seller { Name = dto.Name };
                _context.Sellers.Add(shop);
            }

            await _context.SaveChangesAsync();
        }
    }
}
