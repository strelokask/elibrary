﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using elibrary.Areas.Administration.Models;
using elibrary.Data;
using Microsoft.EntityFrameworkCore;

namespace elibrary.Areas.Administration.Services
{
    public class NumberService
    {
        private readonly ApplicationDbContext _context;

        public NumberService(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<NumberDTO>> GetCollectionDTOAsync()
        {
            return await _context.MyNumbers.Select(x => new NumberDTO
            {
                Id = x.Id,
                PhoneNumber = x.Number,
                Date = x.Date,
                Sum = x.Sum,
                Balance = x.Balance
            })
            .ToListAsync();
        }

        public async Task<NumberDTO> GetDTOAsync(int id)
        {
            return await _context.MyNumbers.Select(x => new NumberDTO
            {
                Id = x.Id,
                PhoneNumber = x.Number,
                Date = x.Date,
                Sum = x.Sum,
                Balance = x.Balance
            }).FirstAsync();
        }

        public async Task DeleteAsync(int id)
        {
            var a = await _context.MyNumbers.Where(x => x.Id == id).FirstAsync();

            _context.MyNumbers.Remove(a);

            _context.SaveChanges();
        }

        public async Task Save(NumberDTO dto)
        {
            if (dto.Id > 0)
            {
                var item = await _context.MyNumbers
                .Where(x => x.Id == dto.Id)
                .FirstAsync();

                item.Id = dto.Id;
                item.Number = dto.PhoneNumber;
                item.Date = dto.Date;
                item.Sum = dto.Sum;
                item.Balance = dto.Balance;

                _context.MyNumbers.Update(item);
            }
            else
            {
                var item = new elibrary.Data.MyNumber();

                item.Id = dto.Id;
                item.Number = dto.PhoneNumber;
                item.Date = dto.Date;
                item.Sum = dto.Sum;
                item.Balance = dto.Balance;


                _context.MyNumbers.Add(item);
            }

            await _context.SaveChangesAsync();
        }
    }
}
