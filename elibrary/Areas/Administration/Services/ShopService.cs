﻿using elibrary.Areas.Administration.Models;
using elibrary.Data;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace elibrary.Areas.Administration.Services
{
    public class ShopService
    {
        private readonly ApplicationDbContext _context;
        public ShopService(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<ShopDTO>> GetCollectionDTOAsync()
        {
            return await _context.Shops
                .Select(x => new ShopDTO { Id = x.Id, Name = x.Name })
                .ToListAsync();
        }

        public async Task<ShopDTO> GetShopDTOAsync(int id)
        {
            return await _context.Shops
                .Where(x => x.Id == id)
                .Select(x => new ShopDTO { Id = x.Id, Name = x.Name })
                .FirstAsync();
        }

        public async Task Save(ShopDTO dto)
        {
            if (dto.Id > 0)
            {
                var shop = await _context.Shops
                .Where(x => x.Id == dto.Id)
                .FirstAsync();

                shop.Name = dto.Name;
                _context.Shops.Update(shop);
            }
            else
            {
                var shop = new elibrary.Data.Shop { Name = dto.Name };
                _context.Shops.Add(shop);
            }

            await _context.SaveChangesAsync();
        }
        public async Task DeleteAsync(int id)
        {
            var a = await _context.Shops
                .Where(x => x.Id == id)
                .FirstAsync();

            _context.Shops.Remove(a);

            await _context.SaveChangesAsync();
        }
    }
}
