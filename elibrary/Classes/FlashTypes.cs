﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace elibrary.Classes
{
    public class FlashTypes
    {
        public const string Success = "success";

        public const string Error = "error";

        public const string Inform = "info";

        public const string Warn = "warning";
    }
}
