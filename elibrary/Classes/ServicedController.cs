﻿using elibrary.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace elibrary.Classes
{
    public class ServicedController<TService> : DefaultController where TService : ServiceBase
    {
        public TService Service { get; }

        public ServicedController(TService service)
        {
            Service = service;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                Service.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
