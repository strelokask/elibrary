﻿using elibrary.Data.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace elibrary.Services
{
    public class ServiceBase : IDisposable
    {
        protected IUnitOfWork UnitOfWork { get; }

        protected ServiceBase(IUnitOfWork unitOfWork)
        {
            UnitOfWork = unitOfWork;
        }

        ~ServiceBase()
        {
            Dispose(false);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                UnitOfWork.Dispose();
            }
        }

        protected Tuple<TModel, bool> GetOrCreateEntity<TModel>(IQueryable<TModel> collection
            , Expression<Func<TModel, bool>> where
            ) where TModel : class, new()
        {
            var result = collection.Where(where).FirstOrDefault();
            var isCreated = false;

            if (result == null)
            {
                isCreated = true;
                result = new TModel();
                UnitOfWork.Insert(result);
            }

            return new Tuple<TModel, bool>(result, isCreated);
        }

        /// <summary>
        /// Get entity from collection
        /// or create new entity and add to collection
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <param name="collection"></param>
        /// <param name="where"></param>
        /// <returns></returns>
        protected Tuple<TModel, bool> GetOrCreateEntity<TModel>(ICollection<TModel> collection, Func<TModel, bool> where) where TModel : class, new()
        {
            var isCreated = false;

            var result = collection
                .Where(where)
                .FirstOrDefault();

            if (result is null)
            {
                result  = new TModel();
                isCreated = true;
                collection.Add(result);
            }

            return new Tuple<TModel, bool>(result, isCreated);
        }

    }
}
