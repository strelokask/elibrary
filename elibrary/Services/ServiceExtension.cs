﻿using elibrary.Data.Core;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace elibrary.Services
{
    public static class ServiceExtension
    {
        public static void RegisterServices(this IServiceCollection services)
        {

            services.AddTransient<IUnitOfWork, UnitOfWork>();
            services.AddTransient<Services.ClientService>();
        }
    }
}
