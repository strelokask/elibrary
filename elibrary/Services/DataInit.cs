﻿using elibrary.Data;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace elibrary
{
    internal class DataInit : IDisposable
    {
        private ApplicationDbContext _context;
        private readonly UserManager<User> _userManager;
        private bool _disposed;

        public DataInit(ApplicationDbContext context, UserManager<User> userManager)
        {
            _context = context;
            _userManager = userManager;
        }

        ~DataInit()
        {
            Dispose(false);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool d)
        {
            if (_disposed)
                return;
            if (d)
            {
                _disposed = true;
                _context.Dispose();
                _userManager.Dispose();
            }
        }

        public async Task Initialize()
        {
            await _context.Database.MigrateAsync().ConfigureAwait(false);

            await CreateUserAsync();

            await _context.SaveChangesAsync();
        }


        private async Task CreateUserAsync()
        {
            if (_context.Users.Any(u => u.UserName == "admin"))
            {
                return;
            }

            var user = new User
            {
                UserName = "admin",
                Email = "admin@local",
                EmailConfirmed = true
            };

            await _userManager.CreateAsync(user, "1qaz!QAZ");
        }
    }
}