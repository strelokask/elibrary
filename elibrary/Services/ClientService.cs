﻿using elibrary.Data;
using elibrary.Data.Core;
using elibrary.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace elibrary.Services
{
    public class ClientService : ServiceBase
    {
        private ILogger<ClientService> _logger;

        public ClientService(IUnitOfWork unitOfWork, ILogger<ClientService> logger) : base(unitOfWork)
        {
            _logger = logger;
        }

        public IEnumerable<Shop> GetShopsAsync() => UnitOfWork.Select<Shop>().ToList();

        public IEnumerable<Seller> GetSellersAsync() => GetCollectionOfEntityAsync<Seller>().ToList();

        public IEnumerable<Tuple<int, string>> GetNumbersAsync() => GetCollectionOfEntityAsync<MyNumber>()
                .Select(x => new Tuple<int, string>(x.Id, x.Number))
                .ToList();

        public async Task<IEnumerable<ClientDTO>> GetCollectionDTOAsync(DateTime ds, DateTime de, int id)
        {
            _logger.LogInformation($"Получаем записи из бд ");
            return await GetCollectionOfEntityAsync<Client>()
                .Include(x => x.MyNumber)
                .Include(x => x.Seller)
                .Include(x => x.Shop)
                .Where(x => x.Date >= ds && x.Date <= de && ( id == 0 || x.MyNumberId == id))
                .Select(x => new ClientDTO
                {
                    Id = x.Id,
                    Number = x.Number,
                    Date = x.Date,
                    Sum = x.Sum,
                    Profit = x.Profit,
                    IsDelivered = x.IsDelivered,
                    IsChecked = x.IsChecked,
                    ShopName = x.Shop.Name,
                    SellerName = x.Seller.Name,
                    MyNumber = x.MyNumber.Number

                })
                .OrderBy(x => x.MyNumber)
                .OrderBy(x => x.ShopName)
                .OrderBy(x => x.SellerName)
                .OrderBy(x => x.Number)
                .ToListAsync();
        }

        public async Task DeleteAsync(int id)
        {
            UnitOfWork.Delete<Client>(id);

            await UnitOfWork.CommitAsync();
        }

        public async Task Save(Client dto)
        {
            var entity = GetOrCreateEntity<Client>(UnitOfWork.Select<Client>().AsQueryable(), x => x.Id == dto.Id);

            FillPropertyExtension.Fill<Client>(entity.Item1, dto);
            
            await UnitOfWork.CommitAsync();
        }

        public async Task<Client> GetDTOAsync(int id)
        {
            return await GetCollectionOfEntityAsync<Client>()
                .Include(x => x.MyNumber)
                .Include(x => x.Seller)
                .Include(x => x.Shop)
                .Where(x => x.Id == id)
                .FirstAsync();
        }

        private IQueryable<T> GetCollectionOfEntityAsync<T>() where T : class
        {
            return UnitOfWork.Select<T>();
        }
    }
}
