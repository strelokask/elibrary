﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using elibrary.Classes;
using elibrary.Data;
using elibrary.Models;
using elibrary.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace elibrary.Controllers
{
    public class ClientController : DefaultController
    {
        private ClientService _service;
        private ILogger<ClientController> _logger;

        public ClientController(ClientService clientService, ILogger<ClientController> logger)
        {
            _service = clientService;
            _logger = logger;
        }

        public async Task<IActionResult> Index(int? numberId, string dateStart = null, string dateEnd = null)
        {
            var ds = !string.IsNullOrEmpty(dateStart) ? DateTime.ParseExact(dateStart, "dd.MM.yyyy H:mm", System.Globalization.CultureInfo.CurrentCulture) : DateTime.Now.AddMonths(-2);
            var de = !string.IsNullOrEmpty(dateEnd) ? DateTime.ParseExact(dateEnd, "dd.MM.yyyy H:mm", System.Globalization.CultureInfo.CurrentCulture) : DateTime.Now;
            var nId = numberId ?? 0;

            try
            {
                var model = await _service.GetCollectionDTOAsync(ds, de, nId);

                ViewBag.DateStart = ds.ToString("dd.MM.yyyy H:mm");
                ViewBag.DateEnd = de.ToString("dd.MM.yyyy H:mm");
                ViewBag.NumberId= nId;
                ViewBag.Numbers = _service.GetNumbersAsync();

                return View(model);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Упал в метод Index: {ex.Message}");
                return Redirect("/Home/Error");
            }
        }
        public IActionResult Create()
        {
            ViewData["Title"] = "Добавление записи";
            ViewBag.Shops = _service.GetShopsAsync();
            ViewBag.Sellers = _service.GetSellersAsync();
            ViewBag.Numbers = _service.GetNumbersAsync();
            return View(new Client() { Date = DateTime.Now});
        }

        public async Task<IActionResult> Edit(int id)
        {
            ViewData["Title"] = "Изменение записи";
            ViewBag.Shops = _service.GetShopsAsync();
            ViewBag.Sellers =  _service.GetSellersAsync();
            ViewBag.Numbers = _service.GetNumbersAsync();
            var model = await _service.GetDTOAsync(id);

            return View(nameof(Create), model);
        }

        public async Task<IActionResult> Delete(int id)
        {
            await _service.DeleteAsync(id);
            return RedirectToAction(nameof(Index));
        }

        [HttpPost]
        [ActionName(nameof(Edit))]
        public async Task<IActionResult> EditPost(Client dto)
        {
            if (ModelState.IsValid)
            {
                await _service.Save(dto);
                //_flasher.Flash(FlashTypes.Success, "Данные сохранены");

                return RedirectToAction(nameof(Index));
            }
            ViewData["Title"] = "Изменение/Добавление записи";  
            ViewBag.Shops = _service.GetShopsAsync();
            ViewBag.Sellers = _service.GetSellersAsync();
            ViewBag.Numbers = _service.GetNumbersAsync();
            return View(nameof(Create), dto);
        }
    }
}