﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace elibrary.Migrations
{
    public partial class client : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "client",
                schema: "dbo",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    number = table.Column<string>(nullable: true),
                    create_date = table.Column<DateTime>(type: "datetime", nullable: false, defaultValueSql: "(getdate())"),
                    sum = table.Column<int>(nullable: false),
                    profit = table.Column<int>(nullable: false),
                    is_delivered = table.Column<bool>(nullable: false),
                    is_checked = table.Column<bool>(nullable: false),
                    shop_id = table.Column<int>(nullable: false),
                    seller_id = table.Column<int>(nullable: false),
                    my_number_id = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_client", x => x.id);
                    table.ForeignKey(
                        name: "FK_client_my_number_my_number_id",
                        column: x => x.my_number_id,
                        principalSchema: "dbo",
                        principalTable: "my_number",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_client_seller_seller_id",
                        column: x => x.seller_id,
                        principalSchema: "dbo",
                        principalTable: "seller",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_client_shop_shop_id",
                        column: x => x.shop_id,
                        principalSchema: "dbo",
                        principalTable: "shop",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_client_my_number_id",
                schema: "dbo",
                table: "client",
                column: "my_number_id");

            migrationBuilder.CreateIndex(
                name: "IX_client_seller_id",
                schema: "dbo",
                table: "client",
                column: "seller_id");

            migrationBuilder.CreateIndex(
                name: "IX_client_shop_id",
                schema: "dbo",
                table: "client",
                column: "shop_id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "client",
                schema: "dbo");
        }
    }
}
