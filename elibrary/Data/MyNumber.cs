﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace elibrary.Data
{
    public class MyNumber
    {
        public int Id { get; set; }
        public string Number { get; set; }
        public DateTime Date { get; set; }
        public int Sum { get; set; }
        public int Balance { get; set; }

        public ICollection<Client> Clients { get; set; } = new HashSet<Client>();
    }
}
