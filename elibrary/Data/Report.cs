﻿using elibrary.Data.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace elibrary.Data
{
    public class Report : Entity
    {
        public string PhoneNumber { get; set; }
        public DateTime Date { get; set; }
        public int Sum { get; set; }
        public string Comment { get; set; }

        public User Modifier { get; set; }

        public int OperationId { get; set; }
        public Operation Operation { get; set; }
    }
}
