﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using elibrary.Data.Configurations;
using elibrary.Data.Core;
using elibrary.Data.Relationships;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace elibrary.Data
{
    public class ApplicationDbContext : IdentityDbContext
        <User, Role, int, UserClaim, RelUserRole, UserLogin, RoleClaim, UserToken>
        //<User, Role, int>
        //<User>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        public virtual DbSet<User> ApplicationUsers { get; set; }
        public virtual DbSet<Role> ApplicationRoles { get; set; }
        public virtual DbSet<RelUserRole> RelUserRoles { get; set; }

        public virtual DbSet<Operation> Operations { get; set; }
        public virtual DbSet<Report> Reports { get; set; }
        public virtual DbSet<Shop> Shops { get; set; }


        public virtual DbSet<Seller> Sellers { get; set; }
        public virtual DbSet<MyNumber> MyNumbers{ get; set; }
        public virtual DbSet<Client> Clients { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.HasDefaultSchema("dbo");

            builder.AddConfiguration(new UserConfiguration());
            builder.AddConfiguration(new RoleConfiguration());
            builder.AddConfiguration(new RelUserRoleConfiguration());
            builder.AddConfiguration(new UserLoginConfiguration());
            builder.AddConfiguration(new UserTokenConfiguration());

            builder.AddConfiguration(new ReportConfiguration());
            builder.AddConfiguration(new OperationConfiguration());

            builder.AddConfiguration(new ShopConfiguration());
            builder.AddConfiguration(new SellerConfiguration());
            builder.AddConfiguration(new MyNumberConfiguration());
            builder.AddConfiguration(new ClientConfiguration());
            //builder.Entity<Report>(config => new ReportConfiguration());
        }
    }
}
