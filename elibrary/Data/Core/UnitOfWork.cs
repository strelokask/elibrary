﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace elibrary.Data.Core
{
    public class UnitOfWork : IUnitOfWork
    {
        private ApplicationDbContext Context { get; }

        public UnitOfWork(ApplicationDbContext context)
        {
            Context = context;
        }

        public async Task CommitAsync()
        {
            await Context.SaveChangesAsync();
        }

        public void Delete<TModel>(TModel model) where TModel : class
        {
            Context.Remove(model);
        }

        public void Delete<TModel>(int id) where TModel : class
        {
            Delete(Context.Find<TModel>(id));
        }

        public void Dispose()
        {
            Context.Dispose();
        }

        public TModel Get<TModel>(int id) where TModel : class
        {
            return Context.Find<TModel>(id);
        }

        public void Insert<TModel>(TModel model) where TModel : class
        {
            Context.Add<TModel>(model);
        }

        public async Task MigrateDatabaseAsync()
        {
            await Context.Database.MigrateAsync();
        }

        public void Update<TModel>(TModel model) where TModel : class
        {
            EntityEntry<TModel> entry = Context.Entry(model);
            if (entry.State != EntityState.Modified)
            {
                entry.State = EntityState.Modified;
            }

        }

        public IQuery<TModel> Select<TModel>() where TModel : class
        {
            return new MyQuery<TModel>(Context.Set<TModel>());
        }
    }
}
