﻿using elibrary.Data.Configurations;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace elibrary.Data.Core
{
    internal static class ModelBuilderExtensions
    {
        public static void AddConfiguration<TEntity>(
            this ModelBuilder modelBuilder,
            DefaultConfiguration<TEntity> entityConfiguration) where TEntity : class
        {   
            modelBuilder.Entity<TEntity>(entityConfiguration.Configure);
        }
    }
}
