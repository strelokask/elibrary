﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace elibrary.Data.Core
{
    public interface IUnitOfWork : IDisposable
    {
        IQuery<TModel> Select<TModel>() where TModel : class;


        TModel Get<TModel>(int id) where TModel : class;

        void Insert<TModel>(TModel model) where TModel : class;

        void Update<TModel>(TModel model) where TModel : class;

        void Delete<TModel>(TModel model) where TModel : class;
        void Delete<TModel>(int id) where TModel : class;

        Task CommitAsync();

        Task MigrateDatabaseAsync();
    }
}
