﻿using System;

namespace elibrary.Data.Core
{
    public interface IEntity<T>
    {
        T Id { get; set; }
        bool IsDeleted { get; set; }
        DateTime CreateDate { get; set; }
        DateTime ModifyDate { get; set; }
        T ModifierId { get; set; }
    }
}
    