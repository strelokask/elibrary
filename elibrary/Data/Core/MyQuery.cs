﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace elibrary.Data.Core
{
    public class MyQuery<T> : IQuery<T>
    {
        public Type ElementType => Set.ElementType;

        public Expression Expression => Set.Expression;

        public IQueryProvider Provider => Set.Provider;

        private IQueryable<T> Set;

        public MyQuery(IQueryable<T> set)
        {
            Set = set;
        }

        public IEnumerator<T> GetEnumerator()
        {
            return Set.GetEnumerator();
        }

        public IQuery<TResult> Select<TResult>(Expression<Func<T, TResult>> selector)
        {
            return new MyQuery<TResult>(Set.Select(selector));
        }

        public IQuery<T> Where(Expression<Func<T, bool>> condition)
        {
            return new MyQuery<T>(Set.Where(condition));
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}
