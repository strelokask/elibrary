﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace elibrary.Data.Core
{
    public static class FillPropertyExtension
    {
        public static void Fill<TModel>(TModel model, TModel value) where TModel : class, new()
        {
            if (model == null)
            {
                throw new ArgumentNullException("Exeption in the filling: model is null");
            }
            if (value is null)
            {
                value = new TModel();
            }

            PropertyInfo[] properties = model.GetType().GetProperties();
            foreach (var property in properties)
            {
                SetValue(model, property, property.GetValue(value));
            }
        }

        private static void SetValue(object target, PropertyInfo info, object value)
        {
            PropertyInfo propertyInfo = target.GetType().GetProperty(info.Name);

            if (propertyInfo != null)
            {
                propertyInfo.SetValue(target, value);
            }
        }
    }
}
