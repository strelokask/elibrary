﻿using System;
using System.Linq;
using System.Linq.Expressions;

namespace elibrary.Data.Core
{
    public interface IQuery<T> : IQueryable<T>
    {
        IQuery<TResult> Select<TResult>(Expression<Func<T, TResult>> selector);
        IQuery<T> Where(Expression<Func<T, bool>> condition);
    }
}
