﻿using elibrary.Data.Core;
using elibrary.Data.Relationships;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace elibrary.Data
{
    public class Role : IdentityRole<int>, IEntity<int>
    {
        public bool IsDeleted      {get;set;}
        public DateTime CreateDate {get;set;}
        public DateTime ModifyDate {get;set;}
        public int ModifierId { get; set; }

        public ICollection<RelUserRole> Users { get; set; } = new HashSet<RelUserRole>();
    }
}
