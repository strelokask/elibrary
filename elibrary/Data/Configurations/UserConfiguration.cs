﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace elibrary.Data.Configurations
{
    class UserConfiguration : DefaultConfiguration<User>
    {
        public override void Configure(EntityTypeBuilder<User> entity)
        {
            entity.ToTable("users").HasKey(e => e.Id);

            entity.HasIndex(e => e.IsDeleted);

            entity.HasIndex(e => e.UserName);

            entity.HasIndex(e => e.PasswordHash);

            entity.HasIndex(e => new { e.CreateDate, e.ModifyDate });

            entity.Property(e => e.Id).HasColumnName("id").UseSqlServerIdentityColumn();

            entity.Property(e => e.AccessFailedCount).HasColumnName("AccessFailedCount");
            entity.Property(e => e.ConcurrencyStamp).HasColumnName("ConcurrencyStamp");
            entity.Property(e => e.Email).HasColumnName("Email");
            entity.Property(e => e.EmailConfirmed).HasColumnName("EmailConfirmed");
            entity.Property(e => e.LockoutEnabled).HasColumnName("LockoutEnabled");
            entity.Property(e => e.LockoutEnd).HasColumnName("LockoutEnd");
            entity.Property(e => e.NormalizedEmail).HasColumnName("NormalizedEmail");
            entity.Property(e => e.NormalizedUserName).HasColumnName("NormalizedUserName");
            entity.Property(e => e.PhoneNumber).HasColumnName("PhoneNumber");
            entity.Property(e => e.PhoneNumberConfirmed).HasColumnName("PhoneNumberConfirmed");
            entity.Property(e => e.SecurityStamp).HasColumnName("SecurityStamp");
            entity.Property(e => e.TwoFactorEnabled).HasColumnName("TwoFactorEnabled");

            entity.Property(e => e.CreateDate)
                .HasColumnName("create_date")
                .HasColumnType("datetime")
                .HasDefaultValueSql("(getdate())");

            entity.Property(e => e.IsDeleted).HasColumnName("is_deleted");

            entity.Property(e => e.UserName)
                .HasColumnName("username")
                //.HasMaxLength(64)
                ;

            entity.Property(e => e.ModifyDate)
                .HasColumnName("modify_date")
                .HasColumnType("datetime")
                .HasDefaultValueSql("(getdate())");

            entity.Property(e => e.PasswordHash)
                .HasColumnName("password")
                ;
        }
    }
}
