﻿using elibrary.Data.Relationships;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace elibrary.Data.Configurations
{
    class RelUserRoleConfiguration : DefaultConfiguration<RelUserRole>
    {
        public override void Configure(EntityTypeBuilder<RelUserRole> entity)
        {
            entity.ToTable("rel_user_role")
                .HasKey(e => new { e.UserId, e.RoleId});

            entity.Property(e => e.UserId)
                .HasColumnName("user_id");
                                          
            entity.Property(e => e.RoleId)
                .HasColumnName("role_id");

            entity.HasOne(d => d.User)
                .WithMany(p => p.Roles)
                .HasForeignKey(d => d.UserId)
                .OnDelete(DeleteBehavior.Restrict)
                ;

            entity.HasOne(d => d.Role)
                .WithMany(p => p.Users)
                .HasForeignKey(d => d.RoleId)
                .OnDelete(DeleteBehavior.Restrict)
                ;
        }
    }
}
