﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace elibrary.Data.Configurations
{
    class ClientConfiguration : DefaultConfiguration<Client>
    {
        public override void Configure(EntityTypeBuilder<Client> entity)
        {
            entity.ToTable("client");

            entity.HasKey(e => e.Id);

            entity.Property(e => e.Id)
                .HasColumnName("id");

            entity.Property(e => e.Number)
                .HasColumnName("number");

            entity.Property(e => e.Date)
                .HasColumnName("create_date")
                .HasColumnType("datetime")
                .HasDefaultValueSql("(getdate())");

            entity.Property(e => e.Sum)
                .HasColumnName("sum");

            entity.Property(e => e.Profit)
                .HasColumnName("profit");

            entity.Property(e => e.IsDelivered)
                .HasColumnName("is_delivered");

            entity.Property(e => e.IsChecked)
                .HasColumnName("is_checked");

            entity.Property(e => e.ShopId)
                .HasColumnName("shop_id");

            entity.Property(e => e.SellerId)
                .HasColumnName("seller_id");

            entity.Property(e => e.MyNumberId)
                .HasColumnName("my_number_id");

            entity.HasOne(e => e.MyNumber)
                .WithMany(x => x.Clients)
                .HasForeignKey(e => e.MyNumberId)
                .OnDelete(DeleteBehavior.Restrict);

            entity.HasOne(e => e.Seller)
                .WithMany(x => x.Clients)
                .HasForeignKey(e => e.SellerId)
                                .OnDelete(DeleteBehavior.Restrict);

            entity.HasOne(e => e.Shop)
                .WithMany(x => x.Clients)
                .HasForeignKey(e => e.ShopId)
                                .OnDelete(DeleteBehavior.Restrict);

        }
    }
}
