﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace elibrary.Data.Configurations
{
    class ReportConfiguration : DefaultConfiguration<Report>
    {
        public override void Configure(EntityTypeBuilder<Report> entity)
        {
            entity.ToTable("report");

            entity.HasKey(e => e.Id);

            entity.HasIndex(e => e.CreateDate);

            entity.HasIndex(e => e.IsDeleted);

            entity.HasIndex(e => e.ModifyDate);

            entity.Property(e => e.Id)
                .HasColumnName("id")
                .UseSqlServerIdentityColumn();

            entity.Property(e => e.CreateDate)
                .HasColumnName("create_date")
                .HasColumnType("datetime")
                .HasDefaultValueSql("(getdate())");

            entity.Property(e => e.IsDeleted).HasColumnName("is_deleted");

            entity.Property(e => e.ModifierId).HasColumnName("modifier_id");

            entity.Property(e => e.ModifyDate)
                .HasColumnName("modify_date")
                .HasColumnType("datetime")
                .HasDefaultValueSql("(getdate())");

            entity.Property(e => e.PhoneNumber)
                .HasColumnName("phone_number");

            entity.Property(e => e.Date)
                .HasColumnName("date")
                .HasColumnType("datetime")
                .HasDefaultValueSql("(getdate())");

            entity.Property(e => e.Sum)
                .HasColumnName("sum");

            entity.Property(e => e.Comment)
                .HasColumnName("comment")
                .HasMaxLength(400);

            entity.Property(e => e.OperationId)
                .IsRequired()
                .HasColumnName("operation_id");

            entity.HasOne(e => e.Operation)
                .WithMany(x => x.Reports)
                .HasForeignKey(e => e.OperationId)
                .OnDelete(DeleteBehavior.Restrict);

            entity.HasOne(e => e.Modifier)
                .WithMany(x => x.Reports)
                .HasForeignKey(e => e.ModifierId)
                .OnDelete(DeleteBehavior.Restrict);
        }
    }
}
