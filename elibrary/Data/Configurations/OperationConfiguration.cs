﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace elibrary.Data.Configurations
{
    class OperationConfiguration : DefaultConfiguration<Operation>
    {
        public override void Configure(EntityTypeBuilder<Operation> entity)
        {
            entity.ToTable("operation");
                
            entity.HasKey(e => e.Id);

            entity.HasIndex(e => e.CreateDate);

            entity.HasIndex(e => e.IsDeleted);

            entity.HasIndex(e => e.ModifyDate);

            entity.Property(e => e.Id)
                .HasColumnName("id")
                .UseSqlServerIdentityColumn();

            entity.Property(e => e.IsDeleted).HasColumnName("is_deleted");

            entity.Property(e => e.ModifierId).HasColumnName("modifier_id");

            entity.Property(e => e.CreateDate)
                .HasColumnName("create_date")
                .HasColumnType("datetime")
                .HasDefaultValueSql("(getdate())");

            entity.Property(e => e.ModifyDate)
                .HasColumnName("modify_date")
                .HasColumnType("datetime")
                .HasDefaultValueSql("(getdate())");

            entity.Property(e => e.Name)
                .HasColumnName("name")
                .HasMaxLength(400);

            entity.HasOne(e => e.Modifier)
                .WithMany(x => x.Operations)
                .HasForeignKey(e => e.ModifierId)
                .OnDelete(DeleteBehavior.Restrict);
        }
    }
}
