﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace elibrary.Data.Configurations
{
    class UserTokenConfiguration : DefaultConfiguration<UserToken>
    {
        public override void Configure(EntityTypeBuilder<UserToken> entity)
        {
            entity.ToTable("user_token");

            entity.HasKey(e => new { e.UserId, e.LoginProvider });

            entity.Property(e => e.UserId).HasColumnName("user_id");
            entity.Property(e => e.LoginProvider).HasColumnName("login_provider");

            entity.HasOne(d => d.User)
                .WithMany(p => p.UserTokens)
                .HasForeignKey(d => d.UserId)
                .OnDelete(DeleteBehavior.Restrict)
                //.HasConstraintName("fka363de6540580429")
                ;
        }
    }
}
