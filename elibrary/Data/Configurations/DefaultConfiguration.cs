﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace elibrary.Data.Configurations
{
    internal abstract class DefaultConfiguration<TEntity> where TEntity : class
    {
        public abstract void Configure(EntityTypeBuilder<TEntity> entity);
    }
}
