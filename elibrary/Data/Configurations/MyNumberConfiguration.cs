﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace elibrary.Data.Configurations
{
    class MyNumberConfiguration : DefaultConfiguration<MyNumber>
    {
        public override void Configure(EntityTypeBuilder<MyNumber> entity)
        {
            entity.ToTable("my_number");
            entity.HasKey(e => e.Id);
            entity.Property(e => e.Id).HasColumnName("id");
            entity.Property(e => e.Number).HasColumnName("number");
            entity.Property(e => e.Date)
                .HasColumnName("create_date")
                .HasColumnType("datetime")
                .HasDefaultValueSql("(getdate())");
            entity.Property(e => e.Sum).HasColumnName("sum");
            entity.Property(e => e.Balance).HasColumnName("balance");

        }
    }
}
