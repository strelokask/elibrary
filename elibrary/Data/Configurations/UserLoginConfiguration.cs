﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace elibrary.Data.Configurations
{
    class UserLoginConfiguration : DefaultConfiguration<UserLogin>
    {
        public override void Configure(EntityTypeBuilder<UserLogin> entity)
        {
            entity.ToTable("user_login");

            entity.HasKey(e => new { e.UserId, e.LoginProvider });

            entity.Property(e => e.UserId).HasColumnName("user_id");
            entity.Property(e => e.LoginProvider).HasColumnName("login_provider");
            entity.Property(e => e.ProviderDisplayName).HasColumnName("provider_display_name");
            entity.Property(e => e.ProviderKey).HasColumnName("provider_key");

            entity.HasOne(d => d.User)
                .WithMany(p => p.UserLogins)
                .HasForeignKey(d => d.UserId)
                .OnDelete(DeleteBehavior.Restrict)
                //.HasConstraintName("fka363de6540580429")
                ;
        }
    }
}
