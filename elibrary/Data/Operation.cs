﻿using elibrary.Data.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace elibrary.Data
{
    public class Operation : Entity
    {
        public string Name { get; set; }

        public virtual User Modifier { get; set; }

        public ICollection<Report> Reports { get; set; } = new HashSet<Report>();
    }
}
