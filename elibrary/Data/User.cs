﻿using elibrary.Data.Core;
using elibrary.Data.Relationships;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace elibrary.Data
{
    public partial class User : IdentityUser<int>, IEntity<int>
    {
        public bool IsDeleted { get;set;}
        public DateTime CreateDate { get;set;}
        public DateTime ModifyDate {get;set;}
        public int ModifierId { get;set;}

        public ICollection<RelUserRole> Roles { get; set; } = new HashSet<RelUserRole>();
        public ICollection<Operation> Operations { get; set; } = new HashSet<Operation>();
        public ICollection<Report> Reports { get; set; } = new HashSet<Report>();
        public ICollection<UserLogin> UserLogins { get; set; } = new HashSet<UserLogin>();
        public ICollection<UserToken> UserTokens { get; set; } = new HashSet<UserToken>();
    }
}
