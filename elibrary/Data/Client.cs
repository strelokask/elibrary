﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace elibrary.Data
{
    public class Client
    {
        public int Id { get; set; }

        [Display(Name = "Номер клиента")]
        [Required(ErrorMessage ="Заполните номер телефона")]
        [RegularExpression(@"^[+]\d{11}$", ErrorMessage = "Номер телефон не действителен. Пример: +77051234567")]
        public string Number { get; set; }

        [Required(ErrorMessage ="Заполните время закидывания денег")]
        [Display(Name = "Дата закидывания денег")]
        public DateTime Date { get; set; }

        [Display(Name = "Сумма закиданных денег")]
        public int Sum { get; set; }

        [Display(Name = "Прибыль")]
        public int Profit { get; set; }

        [Display(Name = "Доставлено")]
        public bool IsDelivered { get; set; } = false;

        [Display(Name = "Проверено")]
        public bool IsChecked { get; set; } = false;

        public int ShopId { get; set; }
        public Shop Shop { get; set; }

        public Seller Seller { get; set; }
        public int SellerId { get; set; }

        public int MyNumberId { get; set; }
        public MyNumber MyNumber { get; set; }
    }
}
