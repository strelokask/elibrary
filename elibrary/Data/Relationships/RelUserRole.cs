﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace elibrary.Data.Relationships
{
    public class RelUserRole : IdentityUserRole<int>
    {
        //public int Id { get; set; }
        public User User { get; set; }
        public Role Role { get; set; }
    }
}
